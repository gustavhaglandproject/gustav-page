#!/bin/sh
cargo run --bin gen_docs vue-client/openapi.json
yarn \
    --cwd vue-client \
    run openapi-typescript openapi.json \
    -o src/queries/apiTypes.ts \
    -c .prettierrc.json \
    --export-type true \
    -it true \
    --path-params-as-types true