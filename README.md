# Cookbook app
## Idea
To have a database with recipies.
The backend is a rust webserver with a database.
The front end is a react web app.
Functionality includes:
- Saving of recipies
- Export of ingredients to a shopping list
- Registry of ingredients with category
- Shopping list is divided into categories

## Future ideas?
- Mobile friendly version.
- Tracking of ingredients at home?
- Import of recipies from websites (ica, undertian, osv...)
- Recipy categories, favorites, timeline?

## Setup
Install diesel with:

``` cargo install diesel_cli --version="2.0.0-rc.0" --no-default-features --feature "sqlite" ```

Run in server directory:

``` 
echo "DATABASE_URL=file:db/recipies.db" > .env
mkdir db
diesel run migration
```

For client development install yarn, run `yarn build` and copy the contents of the build directory over to `server/static`.

Then start the web server with `cargo run` and voila, a webpage at localhost:9000!