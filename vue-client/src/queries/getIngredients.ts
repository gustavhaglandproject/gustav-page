import { useQuery } from "vue-query"
import { paths } from "./apiTypes"
import fetcher from "./Fetcher"

export const getIngredientsKey = ["ingredients"] as const

const getIngredients = fetcher.path("/api/ingredients").method("get").create()

const useGetIngredients = () =>
  useQuery(getIngredientsKey, async () => getIngredients({}))

export const getIngredientsMock: paths["/api/ingredients"]["get"]["responses"]["200"]["content"]["application/json"] =
  [
    {
      name: "Red onion",
    },
    {
      name: "Cucumber",
    },
  ]

export default useGetIngredients
