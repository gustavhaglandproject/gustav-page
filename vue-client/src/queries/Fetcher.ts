import { Fetcher } from "openapi-typescript-fetch"
import { paths } from "./apiTypes"

const fetcher = Fetcher.for<paths>()

export default fetcher
