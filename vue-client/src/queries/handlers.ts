import {
  DefaultRequestBody,
  PathParams,
  ResponseResolver,
  rest,
  RestContext,
  RestRequest,
} from "msw"
import { paths } from "./apiTypes"
import { getIngredientsMock } from "./getIngredients"
import { getRecipesMock } from "./getRecipes"

const mockGet = <
  Params_2 extends PathParams = PathParams,
  ResponseBody_2 extends DefaultRequestBody = DefaultRequestBody
>(
  path: keyof paths,
  resolver: ResponseResolver<
    RestRequest<never, Params_2>,
    RestContext,
    ResponseBody_2
  >
) => rest.get(path, resolver)

const mockPost = <
  Params_2 extends PathParams = PathParams,
  ResponseBody_2 extends DefaultRequestBody = DefaultRequestBody
>(
  path: keyof paths,
  resolver: ResponseResolver<
    RestRequest<never, Params_2>,
    RestContext,
    ResponseBody_2
  >
) => rest.post(path, resolver)

export const handlers = [
  mockGet("/api/recipes", (req, res, context) => {
    return res(context.status(200), context.json(getRecipesMock))
  }),

  mockPost("/api/recipy", (req, res, ctx) => {
    return res(ctx.status(200), ctx.text("1"))
  }),

  mockGet("/api/ingredients", (_, res, ctx) => {
    return res(ctx.status(200), ctx.json(getIngredientsMock))
  }),
]
