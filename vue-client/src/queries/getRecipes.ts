import { useQuery } from "vue-query"
import { paths } from "./apiTypes"
import fetcher from "./Fetcher"

export const getRecipesKey = ["recipes"] as const

const getRecipes = fetcher.path("/api/recipes").method("get").create()

const useGetRecipes = () =>
  useQuery(getRecipesKey, async () => await getRecipes({}))

export const getRecipesMock: paths["/api/recipes"]["get"]["responses"]["200"]["content"]["application/json"] =
  [
    {
      name: "Lasagne",
    },
    {
      name: "Stew",
    },
  ]

export default useGetRecipes
