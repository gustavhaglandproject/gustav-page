import { useMutation, useQueryClient } from "vue-query"
import fetcher from "./Fetcher"

const addRecipy = fetcher.path("/api/recipy").method("post").create()

const useAddRecipy = () => {
  const queryClient = useQueryClient()
  return useMutation(addRecipy, {
    onSuccess: () => queryClient.invalidateQueries("recipes"),
  })
}

export default useAddRecipy
