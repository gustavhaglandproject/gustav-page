import { createApp } from "vue"
import { VueQueryPlugin } from "vue-query"
import App from "./App.vue"

if (process.env.NODE_ENV === "development") {
  import("./queries/browser")
    .then(({ default: worker }) => worker.start())
    .then(() => createApp(App).use(VueQueryPlugin).mount("#app"))
} else {
  createApp(App).use(VueQueryPlugin).mount("#app")
}
