-- Your SQL goes here

CREATE TABLE recipyingredients (
    hash VARCHAR NOT NULL PRIMARY KEY,
    recipy VARCHAR NOT NULL,
    ingredient VARCHAR NOT NULL,
    FOREIGN KEY(recipy) REFERENCES recipes(name),
    FOREIGN KEY(ingredient) REFERENCES ingredients(name)
);