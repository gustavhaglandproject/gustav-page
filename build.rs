use static_files::NpmBuild;

fn main() -> std::io::Result<()> {
    NpmBuild::new("vue-client")
        .executable("yarn")
        .install()?
        .run("build")?
        .target("vue-client/dist")
        .change_detection()
        .to_resource_dir()
        .build()
}
