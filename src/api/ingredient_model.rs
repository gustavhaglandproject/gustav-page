use crate::schema;
use diesel::{prelude::*, Insertable, QueryResult, Queryable, SqliteConnection};
use serde::{Deserialize, Serialize};
use utoipa::Component;

#[derive(Queryable, Serialize, Component, Insertable, Deserialize)]
#[diesel(table_name = schema::ingredients)]
pub struct Ingredient {
    #[component(example = "Red onion")]
    pub name: String,
}

pub fn get_ingredients(connection: &mut SqliteConnection) -> QueryResult<Vec<Ingredient>> {
    use schema::ingredients::dsl::*;

    ingredients.load::<Ingredient>(connection)
}

pub fn insert_ingredient(
    connection: &mut SqliteConnection,
    ingredient: Ingredient,
) -> QueryResult<usize> {
    use schema::ingredients::dsl::*;

    diesel::insert_into(ingredients)
        .values(ingredient)
        .execute(connection)
}
