use crate::schema;
use diesel::{prelude::*, Insertable, Queryable};
use serde::{Deserialize, Serialize};
use utoipa::Component;

#[derive(Queryable, Serialize, Component, Insertable, Deserialize)]
#[diesel(table_name = schema::recipes)]
pub struct Recipy {
    #[component(example = "Lasagne")]
    pub name: String,
}

pub fn get_recipes(connection: &mut SqliteConnection) -> QueryResult<Vec<Recipy>> {
    use schema::recipes::dsl::*;

    recipes.load::<Recipy>(connection)
}

pub fn insert_recipy(connection: &mut SqliteConnection, recipy: Recipy) -> QueryResult<usize> {
    use schema::recipes::dsl::*;

    diesel::insert_into(recipes)
        .values(recipy)
        .execute(connection)
}
