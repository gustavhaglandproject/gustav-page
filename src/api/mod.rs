use crate::DbPool;
use actix_web::{
    get, post,
    web::{self, Data},
    HttpResponse, Responder, Scope,
};

mod ingredient_model;
mod recipy_model;
use recipy_model::{get_recipes, insert_recipy, Recipy};
use utoipa::OpenApi;

use self::ingredient_model::{get_ingredients, insert_ingredient, Ingredient};

#[utoipa::path(
    context_path =  "/api",
    responses(
        (status=200, description="Get all recipes", body=[Recipy])
    )
)]
#[get("/recipes")]
async fn recipes(pool: Data<DbPool>) -> impl Responder {
    if let Ok(conn) = &mut pool.get() {
        if let Ok(recipes) = get_recipes(conn) {
            return HttpResponse::Ok().json(recipes);
        }
    }
    HttpResponse::InternalServerError().finish()
}

#[utoipa::path(
    context_path =  "/api",
    request_body = Recipy,
    responses(
        (status=200, description="Insert new recipy", body=String)
    )
)]
#[post("/recipy")]
async fn recipy(pool: Data<DbPool>, new_recipy: web::Json<Recipy>) -> impl Responder {
    if let Ok(conn) = &mut pool.get() {
        if let Ok(size) = insert_recipy(conn, new_recipy.into_inner()) {
            return HttpResponse::Ok().body(size.to_string());
        }
    }
    HttpResponse::InternalServerError().finish()
}

#[utoipa::path(
    context_path =  "/api",
    responses(
        (status=200, description="Get all ingredients", body=[Ingredient])
    )
)]
#[get("/ingredients")]
async fn ingredients(pool: Data<DbPool>) -> impl Responder {
    if let Ok(conn) = &mut pool.get() {
        if let Ok(ingredients) = get_ingredients(conn) {
            return HttpResponse::Ok().json(ingredients);
        }
    }
    HttpResponse::InternalServerError().finish()
}

#[utoipa::path(
    context_path = "/api",
    responses(
        (status = 200, description = "Insert new ingredient", body=String)
    )
)]
#[post("/ingredient")]
async fn ingredient(pool: Data<DbPool>, ingredient: web::Json<Ingredient>) -> impl Responder {
    if let Ok(conn) = &mut pool.get() {
        if let Ok(amount) = insert_ingredient(conn, ingredient.into_inner()) {
            return HttpResponse::Ok().body(amount.to_string());
        }
    }
    HttpResponse::InternalServerError().finish()
}

pub fn routes() -> Scope {
    web::scope("/api")
        .service(recipes)
        .service(recipy)
        .service(ingredients)
        .service(ingredient)
}

pub fn docs() -> utoipa::openapi::OpenApi {
    #[derive(OpenApi)]
    #[openapi(
        handlers(recipes, recipy, ingredients, ingredient),
        components(Recipy, Ingredient)
    )]
    struct ApiDoc;

    ApiDoc::openapi()
}
