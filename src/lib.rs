use actix_web::{middleware::Logger, web, App, HttpServer};
use diesel::{r2d2::ConnectionManager, Connection, SqliteConnection};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use serde::Deserialize;
use utoipa_swagger_ui::SwaggerUi;

//To make the schema available
mod api;
pub mod schema;

type DbPool = r2d2::Pool<diesel::r2d2::ConnectionManager<SqliteConnection>>;

include!(concat!(env!("OUT_DIR"), "/generated.rs"));

const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

#[derive(Debug, Deserialize)]
pub struct ConfigAddress {
    pub url: String,
    pub port: u16,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub database_url: String,
    pub address: ConfigAddress,
}

pub fn docs() -> utoipa::openapi::OpenApi {
    api::docs()
}

pub async fn run(config: Config) -> std::io::Result<()> {
    println!("Starting with config: {:?}", config);

    let manager = ConnectionManager::new(&config.database_url);
    let pool = r2d2::Pool::new(manager).expect("Failed to create db pool");

    if let Ok(mut conn) = SqliteConnection::establish(&config.database_url) {
        match conn.run_pending_migrations(MIGRATIONS) {
            Ok(_) => println!("Migration run success"),
            Err(err) => panic!("Migration run failed: {}", err),
        }
    }

    let docs = api::docs();

    HttpServer::new(move || {
        let generated = generate();
        App::new()
            .app_data(web::Data::<DbPool>::new(pool.clone()))
            .service(
                SwaggerUi::new("/swagger-ui/{_:.*}").url("/api-doc/openapi.json", docs.clone()),
            )
            .service(api::routes())
            .service(actix_web_static_files::ResourceFiles::new("/", generated))
            .wrap(Logger::default())
    })
    .bind((config.address.url, config.address.port))?
    .run()
    .await
}
