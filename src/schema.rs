// @generated automatically by Diesel CLI.

diesel::table! {
    ingredients (name) {
        name -> Text,
    }
}

diesel::table! {
    recipes (name) {
        name -> Text,
    }
}

diesel::table! {
    recipyingredients (hash) {
        hash -> Text,
        recipy -> Text,
        ingredient -> Text,
    }
}

diesel::joinable!(recipyingredients -> ingredients (ingredient));
diesel::joinable!(recipyingredients -> recipes (recipy));

diesel::allow_tables_to_appear_in_same_query!(
    ingredients,
    recipes,
    recipyingredients,
);
