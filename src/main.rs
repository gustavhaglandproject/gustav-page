use cookbook::run;
use env_logger::Env;
use clap::Parser;

const DEFAULT_CONFIG_PATH: &str = "/etc/cookbook/config.yml";

#[derive(Parser)]
struct Args {
    #[clap(long, short)]
    config_path: Option<String>
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(Env::default().default_filter_or("info"));

    let args = Args::parse();

    let config_path = match args.config_path {
        Some(path) => path,
        None => DEFAULT_CONFIG_PATH.to_string()
    };

    run(serde_yaml::from_str(&tokio::fs::read_to_string(config_path).await?).unwrap()).await
}
